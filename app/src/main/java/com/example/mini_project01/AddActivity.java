package com.example.mini_project01;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class AddActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnCancel,btnSave;
    EditText title,contact;
    String Title,Contact;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        intent = getIntent();

        btnCancel = findViewById(R.id.btn_cancel);
        btnSave = findViewById(R.id.btn_save);

        title = findViewById(R.id.title);
        contact = findViewById(R.id.contact);

        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_save:
                Title = title.getText().toString();
                Contact = contact.getText().toString();
                if(Title.isEmpty() || Contact.isEmpty()){
                    Toast.makeText(AddActivity.this,"No Contact",Toast.LENGTH_SHORT).show();
                }else{
                    intent.putExtra("title",Title);
                    intent.putExtra("contact",Contact);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            case R.id.btn_cancel:
                setResult(RESULT_CANCELED, intent);
                finish();
                break;
            default:
                Toast.makeText(AddActivity.this,"No Contact",Toast.LENGTH_SHORT).show();
                break;
        }
    }
}