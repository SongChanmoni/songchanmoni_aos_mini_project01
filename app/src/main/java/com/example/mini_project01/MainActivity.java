package com.example.mini_project01;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    FrameLayout Gallery,Contact,CallPhone,Profile,NoteBook;
    Intent intent = new Intent();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Gallery     = findViewById(R.id.gallery);
        Contact     = findViewById(R.id.contact);
        CallPhone   = findViewById(R.id.call);
        Profile     = findViewById(R.id.your_profile);
        NoteBook    = findViewById(R.id.notebook);

        Gallery.setOnClickListener(this);
        Contact.setOnClickListener(this);
        CallPhone.setOnClickListener(this);
        Profile.setOnClickListener(this);
        NoteBook.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.gallery:
                intent.setClass(MainActivity.this,GalleryScreen.class);
                startActivity(intent);
                break;
            case R.id.contact:
                intent.setClass(MainActivity.this, ContactActivity.class);
                startActivity(intent);
                break;
            case R.id.call:
                intent.setClass(MainActivity.this,CallActivity.class);
                startActivity(intent);
                break;
            case R.id.your_profile:
                intent.setClass(MainActivity.this,ProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.notebook:
                intent.setClass(MainActivity.this,NoteScreen.class);
                startActivity(intent);
                break;
            default:
                Toast.makeText(this,"clicked",Toast.LENGTH_SHORT).show();
                break;
        }

    }
}